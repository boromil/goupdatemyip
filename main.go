package main

import (
	"context"
	"crypto/tls"
	"flag"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"time"
)

const (
	updateIPURLEnvName      string = "UPDATE_IP_URL"
	updateIPIntervalEnvName string = "UPDATE_IP_INTERVAL"
	updateIPTimeoutEnvName  string = "UPDATE_IP_TIMEOUT"
)

var (
	defaultUpdateIPInterval = 10 * time.Minute
	defaultUpdateIPTimeout  = 45 * time.Second
)

func main() {
	updateIPURL := flag.String("update-ip-url", "", "defines the url to be used for ip update requests")
	updateIPInterval := flag.Duration("update-ip-interval", defaultUpdateIPInterval, "defines the time interval to be used for ip update operaions")
	updateIPTimeout := flag.Duration("update-ip-timeout", defaultUpdateIPTimeout, "defines the timeout to be used for ip update requests")
	flag.Parse()

	if updateIPURLFromEnv := os.Getenv(updateIPURLEnvName); updateIPURLFromEnv != "" {
		updateIPURL = &updateIPURLFromEnv
	}
	if updateIPURL == nil {
		log.Fatalf("no update ip url provided")
	}
	if _, err := url.ParseRequestURI(*updateIPURL); err != nil {
		log.Fatalf("bad update ip url provided: %v", err)
	}
	if updateIPIntervalFromEnv, _ := time.ParseDuration(os.Getenv(updateIPIntervalEnvName)); updateIPIntervalFromEnv > 0 {
		updateIPInterval = &updateIPIntervalFromEnv
	}
	if updateIPTimeoutFromEnv, _ := time.ParseDuration(os.Getenv(updateIPTimeoutEnvName)); updateIPTimeoutFromEnv > 0 {
		updateIPTimeout = &updateIPTimeoutFromEnv
	}

	httpClient := &http.Client{
		Transport: &http.Transport{
			Proxy: nil,
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
				DualStack: true,
			}).DialContext,
			ForceAttemptHTTP2:     true,
			MaxIdleConns:          100,
			IdleConnTimeout:       30 * time.Second,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
		},
		Timeout: 30 * time.Second,
	}

	appCtx, cancelAppCtx := context.WithCancel(context.Background())
	defer cancelAppCtx()

	updateIPTicks := time.NewTicker(*updateIPInterval)
	ipUpdater := newIPUpdater(*updateIPURL, updateIPTicks.C, *updateIPTimeout, httpClient)
	log.Println("staring up updater")
	go ipUpdater.run(appCtx)

	signals := make(chan os.Signal)
	signal.Notify(signals, os.Interrupt, os.Kill)

	<-signals
	updateIPTicks.Stop()
	cancelAppCtx()
}
