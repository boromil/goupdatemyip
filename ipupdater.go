package main

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type doer interface {
	Do(req *http.Request) (resp *http.Response, err error)
}

type ipUpdater struct {
	updateIPURL     string
	updateIPTicks   <-chan time.Time
	updateIPTimeout time.Duration

	httpClient doer
}

func newIPUpdater(
	updateIPURL string,
	updateIPTicks <-chan time.Time,
	updateIPTimeout time.Duration,
	httpClient doer,
) *ipUpdater {
	return &ipUpdater{
		updateIPURL:     updateIPURL,
		updateIPTicks:   updateIPTicks,
		updateIPTimeout: updateIPTimeout,
		httpClient:      httpClient,
	}
}

func (u *ipUpdater) run(ctx context.Context) {
	for {
		select {
		case <-u.updateIPTicks:
			func() {
				updateCtx, updateCtxCancel := context.WithTimeout(ctx, u.updateIPTimeout)
				defer updateCtxCancel()
				if err := u.update(updateCtx); err != nil {
					log.Printf("failed to perform up update: %v", err)
				}
			}()
		case <-ctx.Done():
			return
		}
	}
}

func (u *ipUpdater) update(ctx context.Context) error {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, u.updateIPURL, nil)
	if err != nil {
		return fmt.Errorf("http.NewRequestWithContext: %w", err)
	}

	resp, err := u.httpClient.Do(req)
	if err != nil {
		return fmt.Errorf("u.httpClient.Do: %w", err)
	}
	defer func() {
		io.Copy(ioutil.Discard, resp.Body)
		resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("got bad response code, expected %d, got: %d", http.StatusOK, resp.StatusCode)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("ioutil.ReadAll resp body: %w", err)
	}
	log.Printf("updated external ip to: %s", string(bytes.TrimSpace(data)))

	return nil
}
